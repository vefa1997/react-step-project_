import React from 'react';
import './Card.css';

const Card = (props) => {

    return( <div >
            <h5 className="CardTitle"> {props.name} </h5>
            <p  className="CardText"> {props.text} </p>

        </div>
    )
};
export default Card;
