import React from 'react';
import './Header.css';
import {Link} from 'react-router-dom'
const Header = (props) => {

    return( 
        <div>
                {/*<h2 className="PageTitle">{props.pageTitle}</h2>*/}
            <nav>
                <h4>Logo</h4>

                <ul className="navLink" >
                    <Link to="/actual">
                        <button>Actual</button>
                    </Link>

                    <Link  to="/singlenote">
                        <button>Archive</button>
                    </Link>

                    <Link  to="/create">
                        <button>Create</button>
                    </Link>
                </ul>
            </nav>

        </div>
     )
};
export default Header;