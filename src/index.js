import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router,Switch, Route } from 'react-router-dom';

import './index.css';
import Create from './component/Create/Create';
import SingleNote from './component/SingleNote/SingleNote';

import Card from "./component/Card/Card";
import Home from "./component/Home/Home";
import Header from "./component/Header/Header";

import {Link} from 'react-router-dom'


import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>


      <Router>
          <div >
              <Header />
              <Switch>
                  <Route path="/actual" exact component={Home} />
                  {/*<Route path="/singlenote" component={SingleNote} />*/}
                  <Route path="/create" component={Create} />
              </Switch>

          </div>
      </Router>






      {/*<div className="Card">*/}
      {/*    <div  className="CardInner" >*/}
      {/*    <Card name= "Note title" text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "  />*/}
      {/*    </div>*/}
      {/*    <div  className="CardInner" >*/}
      {/*    <Card  name="Note title  " text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. " />*/}
      {/*    </div>*/}
      {/*    <div  className="CardInner" >*/}
      {/*    <Card    name="Note title" text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.  "  />*/}
      {/*    </div>*/}
      {/*    <div  className="CardInner">*/}
      {/*    <Card  name="Note title" text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.   "  />*/}
      {/*    </div>*/}
      {/*    <div  className="CardInner">*/}
      {/*    <Card  name="Note title" text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "  />*/}
      {/*    </div>*/}
      {/*</div>*/}



  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
